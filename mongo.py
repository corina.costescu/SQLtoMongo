from pymongo import MongoClient

class Mongo:

    def __init__(self, mongoConnection):
        self.host = mongoConnection['host']
        self.port = mongoConnection['port']
        self.user = mongoConnection['user']
        self.pswd = mongoConnection['pswd']
        self.db   = mongoConnection['db']

    def get_connection(self):
        mongoClient = MongoClient('mongodb://' + self.host + ':' + self.port)
        db = mongoClient[self.db]
        return db

