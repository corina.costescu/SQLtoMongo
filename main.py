from database import Database
from mongo import Mongo
import sys

connection = {
    'host': 'localhost',
    'user': 'root',
    'passwd': 'root',
    'db': 'skills'
}

mongo_conn = {
    'host': 'localhost',
    'port': '27017',
    'user': 'root',
    'pswd': 'root',
    'db': 'testdb'
}
db = Database(connection)
cursor = db.get_connection()

cursor.execute("SELECT table_name,column_name,constraint_name, referenced_table_name,referenced_column_name"
               " FROM information_schema.key_column_usage "
               "WHERE referenced_table_schema = 'skills'")
data = cursor.fetchall()

# assume we start from users table
sql = "SELECT * FROM users"


def build_query(nodes=None, parent=None, query=None):
    if parent is None:
        return query
    next_parent = None
    for node in nodes:
        if node['table_name'] == parent and node['used'] == 0:
            query += " LEFT JOIN " + node['referenced_table_name'] + " on " + node['referenced_table_name'] + "." + \
                     node['referenced_column_name'] + " = " + node['table_name'] + "." + node['column_name']
            node['used'] = 1
            next_parent = node['referenced_table_name']
    return build_query(nodes, next_parent, query)


for node in data:
    node['used'] = 0


print(build_query(data, 'users', sql))


# for element in data:
#     if element['table_name'] == 'users' and element['used'] != 1:
#         sql += " LEFT JOIN " + element['referenced_table_name'] + " on " + element['referenced_table_name'] + "." + \
#                element['referenced_column_name'] + " = " + element['table_name'] + "." + element['column_name']
#         element['used'] = 1

#
# def build_query_loop(main_table, data, sql):
#     for element in data:
#         if element['table_name'] == main_table and element['used'] != 1:
#             sql += " LEFT JOIN " + element['referenced_table_name'] + " on " + element['referenced_table_name'] + "." + \
#                    element['referenced_column_name'] + " = " + element['table_name'] + "." + element['column_name']
#             element['used'] = 1
#             return build_query_loop(element['referenced_table_name'], data, sql)
#     return sql
#
#
# build_query_loop('users', data, sql)

print(sql)

sys.exit()

cursor.execute(sql)
users = cursor.fetchall()

for user in users:
    print(user)
# cursor.execute('SELECT email, c.name country_name, s.name studio_name, com.name community_name,' +
#                't.name technology_name, sk.`rank` FROM users u ' +
#                'LEFT JOIN countries c ON c.id = u.country_id ' +
#                'LEFT JOIN studios s ON s.id = u.studio_id ' +
#                'LEFT JOIN communities com ON com.id = u.community_id ' +
#                'LEFT JOIN skills sk ON sk.user_id = u.id ' +
#                'LEFT JOIN technologies t ON t.id = sk.technology_id')

client = Mongo(mongo_conn)
db = client.get_connection()
customers = db.customers.find()
for customer in customers:
    print(customer)

result = cursor.fetchall()
data = {}
technologies = []
user = ''
for row in result:
    if user != row['email']:
        user = row['email']
        technologies = []
        data[user] = {}
        # print('new user')
    for key, value in row.items():
        if key in data[user].keys() and (data[user][key] != value):
            print(type(data[user][key]))
            if type(data[user][key]) is list:
                data[user][key].append(value)
            else:
                temp = data[user][key]
                data[user][key] = []
                data[user][key].append(temp)
                data[user][key].append(value)

        else:
            data[user][key] = value
print(data)




db.customers.insert_many(data)
