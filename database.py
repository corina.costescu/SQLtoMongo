#!/usr/bin/python
from pymysql.cursors import DictCursor

import MySQLdb.cursors


class Database:

    def __init__(self, connection):
        self.host = connection['host']
        self.user = connection['user']
        self.passwd = connection['passwd']
        self.db = connection['db']

    def get_connection(self):
        try:
            db = MySQLdb.connect(self.host, self.user, self.passwd, self.db, cursorclass=MySQLdb.cursors.DictCursor)

            # you must create a Cursor object. It will let
            #  you execute all the queries you need
            cur = db.cursor()

            return cur

        except MySQLdb.Error as err:
            print(err)


